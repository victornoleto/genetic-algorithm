## Algoritmo Genético

[O que é o algoritmo genético?](https://en.wikipedia.org/wiki/Genetic_algorithm)

### Como executar

Criar ambiente virtual: `python3 -m venv venv`

Entrar no ambiente virtual (linux): `source venv/bin/activate`

Instalar dependências: `pip install - r requirements.txt`

Executar: `python app.py`