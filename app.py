from ga.algorithm import GeneticAlgorithm as GA
from ga.selection import SelectionEnum
from ga.crossover import CrossoverEnum
import math

def chromosome_calculate_fitness(genes):
	''' Calcular pontuação do cromossomo '''

	fitness = sum(genes)

	return fitness

chromosome_size = 10

api = GA(
	max_generations = 100,
	population_size = 100,
	chromosome_size = chromosome_size,
	selection_method = SelectionEnum.Tournament,
	crossover_rate = 0.7,
	crossover_method = CrossoverEnum.TwoPoint,
	mutation_rate = 0.05,
	chromosome_calculate_fitness_fn = chromosome_calculate_fitness
)

best_result = api.solve()

print('O melhor resultado (' + str(best_result.fitness) + ') encontrado foi: ', best_result.genes)
