from ga.chromosome import Chromosome
from ga.selection import Selection, SelectionEnum
from ga.crossover import Crossover, CrossoverEnum
from ga import utils
import math
import random
import matplotlib.pyplot as plt

class GeneticAlgorithm:

	def __init__(
		self,
		max_generations = 10,
		population_size = 10,
		chromosome_size = 4,
		selection_method = SelectionEnum.Roulette,
		crossover_rate = 0.7,
		crossover_method = CrossoverEnum.OnePoint,
		mutation_rate = 0.05,
		chromosome_is_valid_fn = Chromosome.default_is_valid_fn,
		chromosome_calculate_fitness_fn = Chromosome.default_calculate_fitness_fn
	):

		# Configurações
		self.max_generations = max_generations
		self.population_size = math.ceil(population_size / 2) * 2
		self.chromosome_size = chromosome_size
		self.selection_method = selection_method
		self.crossover_rate = crossover_rate
		self.crossover_method = crossover_method
		self.mutation_rate = mutation_rate
		self.chromosome_is_valid_fn = chromosome_is_valid_fn
		self.chromosome_calculate_fitness_fn = chromosome_calculate_fitness_fn

		self.__create_starting_population()

	def solve(self):
		''' Iniciar algoritmo '''

		self.generations = []
		self.generation_index = 0

		while self.generation_index < self.max_generations:

			self.__start_generation()

			generation_data = self.__get_generation_data()

			''' print(
				self.generation_index, '\t',
				generation_data['max'], '\t',
				''.join(str(gene) for gene in generation_data['population'][0].genes)
			) '''

			self.generations.append(generation_data)

			self.generation_index += 1

		return self.population[0]

	def plot(self, expected = None):
		''' Plotar resultados '''

		x = range(len(self.generations))

		y = {
			'max': [data['max'] for data in self.generations],
			'min': [data['min'] for data in self.generations],
			'avg': [data['avg'] for data in self.generations]
		}

		plt.plot(x, y['max'], label = 'max')
		plt.plot(x, y['min'], label = 'min')
		plt.plot(x, y['avg'], label = 'avg')

		if expected:
			plt.axhline(y = expected, color = 'r', linestyle = '-')

		plt.legend()
		plt.show()

	def __create_starting_population(self):
		''' Criar população inicial '''

		self.population = []

		while len(self.population) < self.population_size:

			chromosome = Chromosome.generate(
				self.chromosome_size,
				self.chromosome_is_valid_fn,
				self.chromosome_calculate_fitness_fn
			)

			self.population.append(chromosome)

		self.__sort_population()

	def __sort_population(self):
		''' Ordenar população da maior pontuação para a menor '''

		self.population.sort(key = lambda chromosome: chromosome.fitness, reverse = True)

	def __start_generation(self):
		''' Iniciar geração '''

		# A população nesta linha já está ordenada em ordem decrescente do maior fitness para o menor
		selected_chromosomes = Selection.solve(
			self.selection_method,
			self.population,
			self.population_size
		)

		# Obter pares da população selecionada
		pairs = list(utils.chunk(selected_chromosomes, 2))

		for pair in pairs:

			if utils.dice(self.crossover_rate):
				
				# Cromossomos obtidos do crossover
				children_chromosomes = Crossover.solve(
					self.crossover_method,
					pair[0],
					pair[1]
				)

				for chromosome in children_chromosomes:
					chromosome.mutate(self.mutation_rate)

				# Adicionar filhos na lista de cromossomos selecionados
				selected_chromosomes = selected_chromosomes + children_chromosomes

		# População final da geração
		self.population = selected_chromosomes

		self.__sort_population()

	def __get_generation_data(self):
		''' Obter dados da geração '''

		fitness_values = [chromosome.fitness for chromosome in self.population]

		data = {
			'population': self.population.copy(),
			'min': min(fitness_values),
			'max': max(fitness_values),
			'avg': sum(fitness_values) / len(fitness_values)
		}

		return data