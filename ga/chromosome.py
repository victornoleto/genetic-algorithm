from ga import utils
import random

class Chromosome:
	
	def __init__(
		self,
		genes,
		is_valid_fn,
		calculate_fitness_fn,
		parents = None
	):
		self.genes = genes
		self.is_valid_fn = is_valid_fn
		self.calculate_fitness_fn = calculate_fitness_fn
		self.parents = parents

		self.__set()

	def mutate(self, rate):
		''' Executar mutação nos genes do cromossomo '''

		original_genes = self.genes.copy()

		for index, gene in enumerate(self.genes):

			if utils.dice(rate):
				self.genes[index] = 1 if gene == 0 else 0

		if self.genes != original_genes:
			
			# Recalcular pontuação do cromossomo
			self.__set()

	def __set(self):
		''' Setar validade e pontuação do cromossomo '''

		self.is_valid = self.is_valid_fn(self.genes)

		self.fitness = 0

		if self.is_valid:
			self.fitness = self.calculate_fitness_fn(self.genes)

	@staticmethod
	def generate(size, is_valid_fn, calculate_fitness_fn):
		''' Gerar cromossomo '''

		genes = []

		for i in range(size):

			gene = random.randint(0, 1)

			genes.append(gene)

		return Chromosome(genes, is_valid_fn, calculate_fitness_fn)

	@staticmethod
	def default_is_valid_fn(genes):
		return True

	@staticmethod
	def default_calculate_fitness_fn(genes):
		return 1