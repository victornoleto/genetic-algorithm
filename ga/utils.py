import random

def chunk(lst, n):
	''' Fragmenta uma lista em outras listas de tamanho "n" '''

	for i in range(0, len(lst), n):

		yield lst[i:i + n]

def dice(prob):
	''' Retorna um valor booleano de acordo com a probabilidade '''

	i = random.randint(1, 100) / 100

	return i <= prob
