from enum import Enum
from ga.chromosome import Chromosome
from ga import utils
import random
import math

class CrossoverEnum:
	OnePoint = 1
	TwoPoint = 2
	Uniform = 3

class Crossover:

	@staticmethod
	def solve(method, chromosome_a, chromosome_b):
		''' Realizar o crossover entre dois cromossomos '''

		genes_a = chromosome_a.genes
		genes_b = chromosome_b.genes

		offspring = []

		if method == CrossoverEnum.OnePoint:

			point = random.randint(1, len(genes_a) - 2)
			
			offspring = [
				genes_a[:point] + genes_b[point:],
				genes_b[:point] + genes_a[point:]
			]

		elif method == CrossoverEnum.TwoPoint:

			l = len(genes_a)
			half = math.floor(l / 2)

			point_a = random.randint(1, half - 1)
			point_b = random.randint(half, l - 2)

			offspring = [
				genes_a[:point_a] + genes_b[point_a:point_b+1] + genes_a[point_b+1:],
				genes_b[:point_a] + genes_a[point_a:point_b+1] + genes_b[point_b+1:],
			]

		elif method == CrossoverEnum.Uniform:

			offspring = [
				[], []
			]

			prob_a = chromosome_a.fitness / (chromosome_a.fitness + chromosome_b.fitness)

			for i in range(len(genes_a)):

				coin_flip = utils.dice(prob_a)

				offspring[0].append(genes_a[i] if coin_flip else genes_b[i])
				offspring[1].append(genes_b[i] if coin_flip else genes_a[i])

		else:
			raise Exception('Invalid crossover method')

		chromosomes = []
		parents = [chromosome_a, chromosome_b]

		for genes in offspring:

			chromosome = Chromosome(
				genes,
				chromosome_a.is_valid_fn,
				chromosome_a.calculate_fitness_fn,
				parents
			)

			chromosomes.append(chromosome)

		return chromosomes

