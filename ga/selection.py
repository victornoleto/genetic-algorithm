from enum import Enum
import numpy as np
import math
import random as rd

class SelectionEnum(Enum):
	Roulette = 1
	Tournament = 2
	Rank = 3
	Random = 4

class Selection():
	
	@staticmethod
	def solve(method, population, keep):
		''' Obter população selecionada '''

		pop = population.copy()

		selected_population = []

		while len(selected_population) < keep and len(pop) > 0:

			if method == SelectionEnum.Roulette:
				selected = Selection.roulette(pop)

			elif method == SelectionEnum.Tournament:
				selected = Selection.tournament(pop)

			elif method == SelectionEnum.Rank:
				selected = Selection.rank(pop)

			elif method == SelectionEnum.Random:
				selected = Selection.random(pop)

			else:
				raise Exception('Invalid selection method')

			index = pop.index(selected)

			# Remover da população
			pop.pop(index)

			# Adicionar nos selecionados
			selected_population.append(selected)

		return selected_population

	@staticmethod
	def roulette(population):

		sum_fitness = sum([c.fitness for c in population])

		if sum_fitness > 0:

			# Probabilidade de cada fitness
			selection_probs = [c.fitness/sum_fitness for c in population]

			# Obter elemento aleatório na lista (com "peso" do fitness)
			index = np.random.choice(len(population), p=selection_probs)

		else:
			index = np.random.choice(len(population))

		selection = population[index]

		return selection

	@staticmethod
	def tournament(population):

		n = 3
		l = len(population)

		if l > (n*2):

			k = math.floor(len(population) / n)

			tournament_list = rd.sample(population, k)

			# Ordenar lista pela pontuação do cromossomo
			tournament_list.sort(key = lambda chromosome: chromosome.fitness, reverse = True)

			# Selecionar primeiro item da lista
			selection = tournament_list[0]

		else:

			index = np.random.choice(len(population))

			selection = population[index]

		return selection

	@staticmethod
	def rank(population):

		selection_probs = [(len(population) - i) for i, chromossome in population]

		index = np.random.choice(len(population), p=selection_probs)

		selection = population[index]

		return selection

	@staticmethod
	def random(population):

		index = np.random.choice(len(population))

		selection = population[index]

		return selection
